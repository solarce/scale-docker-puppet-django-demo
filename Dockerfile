FROM puppet-agent-3.4

ADD puppet /root/puppet/

RUN apt-get update -y
RUN puppet apply --color=false --modulepath /root/puppet/modules /root/puppet/site.pp 2>&1
