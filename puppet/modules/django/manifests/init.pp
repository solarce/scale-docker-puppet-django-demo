class django {
  package {
    'python-pip':
      ensure => present;

    'Django':
      ensure   => present,
      provider => pip;
  }
}
